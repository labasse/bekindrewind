﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeKindRewind
{
    public class Category
    {
        public Category()
        {

        }
        public int CategoryId { get; set; }
        public string Name { get; set; }
        public DateTime LastUpdate { get; set; }
    }
}
