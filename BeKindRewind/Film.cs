﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeKindRewind
{
    public class Film
    {
        public Film()
        {

        }
        public int FilmId { get; set; }
        public string Title { get; set; }

        public int ActorNum { get; set; }

        public string Description { get; set; }
        public int? ReleaseYear { get; set; }
        public int? LanguageId { get; set; }
        public int? OriginalLanguageId { get; set; }

        public string RentalDuration { get; set; }
        public string RentalRate { get; set; }
        public string Length { get; set; }
        public string ReplacementCost { get; set; }
        public string Rating { get; set; }
        public string SpecialFeatures { get; set; }
    }
}
