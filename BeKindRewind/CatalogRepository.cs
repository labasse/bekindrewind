﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using Dapper;

namespace BeKindRewind
{
    public class CatalogRepository : IDisposable
    {
        private SqlConnection _db;
        private List<Category> _categories;

        public CatalogRepository(string connectionString)
        {
            _db = new SqlConnection(connectionString);
            _categories = new List<Category>();
            _categories.AddRange(_db.Query<Category>("SELECT category_id AS CategoryId, name, last_update AS LastUpdate FROM Category"));
        }

        public void Dispose()
        {
            _db.Close();
        }

        public IEnumerable<Category> Categories => _categories;

        public IEnumerable<Film> GetFilmsByCategory(Category category)
        {
            var fields = new Dictionary<string, string>() {
                { "film_id"             , "FilmId" },
                { "title"               , null },
                { "release_year"        , "ReleaseYear" },
                { "language_id"         , "LanguageId" },
                { "original_language_id", "OriginalLanguageId" },
                { "rental_duration"     , "RentalDuration"},
                { "rental_rate"         , "RentalRate"},
                { "length"              , null},
                { "replacement_cost"    , "ReplacementCost" },
                { "rating"              , null },
                { "special_features"    , "SpecialFeatures" }
            };
            var sbSelect = new StringBuilder();
            var sbGroupBy = new StringBuilder();
            var sep = "[film].[";

            foreach(var pair in fields)
            {
                sbGroupBy.Append(sep);
                sbGroupBy.Append(pair.Key);
                sbGroupBy.Append("]");

                sbSelect.Append(sep);
                sbSelect.Append(pair.Key);
                sbSelect.Append("]");
                if (pair.Value!=null)
                {
                    sbSelect.Append(" AS [");
                    sbSelect.Append(pair.Value);
                    sbSelect.Append("]");
                }
                sep = ", [film].[";
            }

            return _db.Query<Film>($@"
                SELECT {sbSelect.ToString()}, COUNT(film_actor.actor_id) AS ActorNum
                FROM film 
                INNER JOIN film_category ON film_category.film_id = film.film_id
                INNER JOIN film_actor ON film_actor.film_id = film.film_id
                WHERE film_category.category_id = @CategoryId
                GROUP BY {sbGroupBy.ToString()}",
                category
            );
        }
    }
}
