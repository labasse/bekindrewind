﻿using BeKindRewind;
using System;
using System.Linq;

namespace BeKindRewindConsole
{
    class Program
    {
        public const string connectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\syl\source\bekindrewind\BeKindRewind\data\sakila.mdf;Integrated Security=True;Connect Timeout=30";
        static void Main(string[] args)
        {
            bool ended = false;

            using(var catalogRepo = new CatalogRepository(connectionString))
            {
                while (!ended)
                {
                    int index = 0;

                    foreach (var category in catalogRepo.Categories)
                    {
                        Console.WriteLine($"{++index} {category.Name}");
                    }
                    Console.WriteLine($"{++index} Quitter");
                    Console.Write("Votre choix : ");

                    var choice = Console.ReadLine();
                    int choiceNum;

                    if(int.TryParse(choice, out choiceNum))
                    {
                        if(choiceNum==index)
                        {
                            ended = true;
                        }
                        else if(1<=choiceNum && choiceNum < index)
                        {
                            var category = catalogRepo.Categories.ElementAt(choiceNum - 1);

                            Console.WriteLine("Films de cette catégorie :");
                            foreach(var film in catalogRepo.GetFilmsByCategory(category).Take(10))
                            {
                                Console.WriteLine($"- {film.Title} ({film.ActorNum})");
                            }
                        }
                    }
                    else
                    {
                        Console.Error.WriteLine("Choix non valide");
                    }
                }
            }
        }
    }
}
